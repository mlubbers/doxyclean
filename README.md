# doxyclean

Use the Cloogle database to filter Clean files to make them suitable for [Doxygen](https://www.doxygen.nl) tagfiles.

## Installation

- Install a recent Clean bundle: https://clean.cs.ru.nl/Clean
- Run `make`

## Usage

In your Doxyfile, make sure to set `INPUT_FILTER` to the path of a compiled version of doxyclean.

## Author
Mart Lubbers

## License
This project is licensed under AGPL v3; see the [LICENSE](/LICENSE) file.

doxyclean uses Cloogle which is maintained by Camil Staps and is licenced under
AGPL v3.

The `Makefile` is adapted from [cloogletags](https://gitlab.com/cloogle/periphery/cloogle-tags), which is licenced under AGPL v3.
