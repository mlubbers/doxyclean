module doxyclean

/**
 * Copyright 2017-2021 the authors (see README.md).
 *
 * This file is part of cloogle-tags.
 *
 * Cloogle-tags is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Cloogle-tags is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-tags. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Clean.Types.Util
import Clean.Doc
import Control.Applicative
import Data.Either
from Data.Func import $, mapSt
import Data.Functor
import Data.Maybe
from Data.Set import :: Set, newSet, instance Foldable Set
import qualified Data.Set
import Text.GenPrint
import Data.Tuple
import Data.List
import System.CommandLine
import System.Directory
import System.Environment
import System.File
import System.FilePath
import System.OSError
import Text

import Cloogle.DB.Factory

Start w
# ([prg:args], w) = getCommandLine w
# (io, w) = stdio w
# (s,w) = mapSt generateCfromModule (map dropExtension args) w
= fclose (foldl (<<<) io $ flatten s) w

generateCfromModule :: !FilePath !*World -> *(![String], !*World)
generateCfromModule path w
#! (funs,rules,gens,typedefs,clss,insts,derivs,clsderivs,(modname,_,imports,_),w) =
	findModuleContents True True False path w
=
	( flip (foldl (flip processImport)) ('Data.Set'.toList imports)
	$ flip (foldl (flip $ processFun "Function")) funs
	$ flip (foldl (flip $ processFun "Macro")) rules
	$ flip (foldl (flip $ processFun "Generic")) gens
	$ flip (foldl (flip $ processTypedef)) typedefs
	$ flip (foldl (flip $ processClass)) clss
	$ []
	, w)
where
	processImport :: !String ![String]-> [String]
	processImport s acc = ["#include <", toDir s, ".dcl>\n":acc]
	where
		toDir = replaceSubString "." (toString pathSeparator)
		

	processFun :: !String !(LocationInModule, FunctionEntry, Set String) ![String] -> [String]
	processFun fkind (lim, fe, _) acc =
		[ "\n"
		, printDoc $ tweakDoc fe.fe_documentation
		, "\n"
		, maybe "" (concat o printType) fe.fe_type
		: acc
		]
	where
		tweakDoc :: (?FunctionDoc) -> FunctionDoc
		tweakDoc ?None = tweakDoc (?Just gDefault{|*|})
		tweakDoc (?Just doc) = {FunctionDoc | doc & description = ?Just $ concat
			[ fromMaybe "" doc.FunctionDoc.description
			, "\n\nOriginal type: ", maybe "" toString fe.fe_type, "\n"
			, "Kind: ", fkind
			]}

		printType :: !Type -> [String]
		printType (Func args res ctx) =
			[ concat $ printType res
			, " ", fromMaybe "" lim.LocationInModule.name, "("
			, concat $ intersperse ", " $ map (concat o printType) args
			,  ");"
			]
		printType (Type n args) = [n]
		printType (Var a) = [a]
		printType (Cons a args) = [a]
		printType (Uniq t) = printType t
		printType (Forall q t tc) = printType t
		printType (Arrow _) = ["arrow"]
		printType (Strict t) = printType t

	processClass :: !(LocationInModule, ClassEntry, [(String, FunctionEntry, Set String)]) ![String] -> [String]
	processClass (lim, ce, members) acc = acc

	processTypedef :: !(LocationInModule, TypeDefEntry) ![String] -> [String]
	processTypedef (lim, te) acc = acc
