BIN:=doxyclean
CLM:=clm
CLMFLAGS:=-h 250M -nr -nt -nortsopts\
	-IL Platform\
	-IL Platform/Deprecated/ArgEnv\
	-I Cloogle\
	-I Cloogle/libcloogle\
	-I Cloogle/regex\
	-I Cloogle/spdx-licenses/src\
	-I Cloogle/compiler/frontend\
	-I Cloogle/compiler/backend\
	-I Cloogle/compiler/main\
	-I Cloogle/compiler/main/Unix\
	-l Cloogle/compiler/backendC/CleanCompilerSources/backend.a

.PHONY: all clean

all: $(BIN)

$(BIN): Cloogle/compiler .FORCE
	$(CLM) $(CLMFLAGS) $@ -o $@

Cloogle/compiler: .FORCE
	$(MAKE) -C Cloogle compiler

clean:
	$(RM) -r 'Clean System Files' $(BIN)

.FORCE:
